add-apt-repository -y ppa:webupd8team/java
apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
apt-get -y install oracle-java7-installer 

echo deb http://debian.datastax.com/community stable main > /etc/apt/sources.list.d/cassandra.sources.list
wget -qO- -L https://debian.datastax.com/debian/repo_key | apt-key add -
apt-get update
apt-get -y install dsc22=2.2.0-1 cassandra=2.2.0

cp /vagrant/cassandra.yaml /etc/cassandra/cassandra.yaml


ip=`ifconfig eth1 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'`

echo $ip

echo $ip > /vagrant/ip.txt

sed -i -e "s/192.168.10.200/$ip/g" /etc/cassandra/cassandra.yaml