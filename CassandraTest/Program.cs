﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cassandra;

namespace CassandraTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var ip = string.Empty;

                {
                    var possible = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\ip.txt");
                    if (File.Exists(possible))
                        ip = Regex.Replace(File.ReadAllText(possible), @"\r\n?|\n", "");
                }

                if (string.IsNullOrEmpty(ip))
                {
                    var possible = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ip.txt");
                    if (File.Exists(possible))
                        ip = Regex.Replace(File.ReadAllText(possible), @"\r\n?|\n", "");
                }

                if (string.IsNullOrEmpty(ip))
                    throw new Exception("Couldn't find vm ip.");

                Cluster.Builder()
                    .AddContactPoint(ip)
                    .WithDefaultKeyspace("test")
                    .Build()
                    .ConnectAndCreateDefaultKeyspaceIfNotExists();
                
                Console.WriteLine("Working!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
